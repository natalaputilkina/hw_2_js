1. Що таке оператори в JavaScript і які їхні типи ви знаєте?

Оператори - це символи або ключові слова, які
виконують операції над операндами. Операндами можуть бути змінні, значення
або вирази.

Типи операторів:
Оператори присвоєння (=)
Оператори порівняння (==, ===, !=, !==, >, >=, <, <=)
Арифметичні оператори (+, -, '\*', /, %, ++, --, '\*\*')
Бітові (порозрядні) оператори
Логічні оператори (&&, ||, !)
Рядкові оператори
Умовний (тернарний) оператор
Оператор кома
Унарні оператори
Оператори відносини
Пріоритет операторів

2. Для чого використовуються оператори порівняння в JavaScript? Наведіть приклади таких операторів.

Оператори порівняння використовуються в логічних інструкціях для визначення рівняння або різниці між змінними або значеннями.

== дорівнює
=== рівне значення та рівний тип
!= не дорівнює
!== не рівне значення або не рівний тип
'>' більше, ніж
'<' менше, ніж
'>=' більше, ніж або дорівнює
'<=' менше, ніж або дорівнює

3. Що таке операції присвоєння в JavaScript? Наведіть кілька прикладів операцій присвоєння.
   Оператор присвоєння = присвоює значення змінній.
   x += y
   x -= y
   x = y
   Якщо необхідно призначити одне значення для кількох змінних, використовуються ланцюжкові оператори присвоєння
